﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
  class Program
  {
    static void Main(string[] args)
    {
      const string path = "books.xml";
      const string outputPath = "booksOutput.xml";

      IBookSerializer serializer = new BookXmlSerializer(path);

      var catalog = serializer.Deserialize();
      if (catalog != null)
      {
        Console.WriteLine("Xml from {0} was successfully deserialized", path);
      }

      serializer.Serialize(catalog, outputPath);
      Console.WriteLine("Object was successfully serialized to {0}", outputPath);
    }
  }
}
