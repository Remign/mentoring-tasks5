﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Task1.Models
{
  [XmlRoot("catalog", Namespace = "http://library.by/catalog")]
  public class Catalog
  {
    [XmlElementAttribute("book")]
    public List<Book> Books { get; set; }

    [XmlAttribute("date", DataType = "date")]
    public DateTime Date { get; set; }
  }
}