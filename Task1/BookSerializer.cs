﻿using System.IO;
using System.Xml.Serialization;
using Task1.Models;

namespace Task1
{
  public interface IBookSerializer
  {
    void Serialize(Catalog catalog, string path = null);

    Catalog Deserialize(string path = null);
  }

  public class BookXmlSerializer : IBookSerializer
  {
    private readonly string _path;

    public BookXmlSerializer(string path)
    {
      _path = path;
    }

    public void Serialize(Catalog catalog, string path = null)
    {
      if (path == null)
      {
        path = _path;
      }

      using (var writer = new StreamWriter(path))
      {
        var serializer = new XmlSerializer(typeof(Catalog));

        serializer.Serialize(writer, catalog);
      }
    }

    public Catalog Deserialize(string path = null)
    {
      if (path == null)
      {
        path = _path;
      }

      Catalog myBooks;

      using (var reader = new StreamReader(path))
      {
        var serializer = new XmlSerializer(typeof(Catalog));

        myBooks = (Catalog)serializer.Deserialize(reader);
      }

      return myBooks;
    }
  }
}