﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task.DB;
using Task.TestHelpers;
using System.Runtime.Serialization;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;

namespace Task
{
	[TestClass]
	public class SerializationSolutions
	{
		Northwind dbContext;

		[TestInitialize]
		public void Initialize()
		{
			dbContext = new Northwind();
		}

    [TestMethod]
    public void SerializationCallbacks()
    {
      dbContext.Configuration.ProxyCreationEnabled = false;

      var tester = new XmlDataContractSerializerTester<IEnumerable<Category>>(new NetDataContractSerializer(), true);
      var categories = dbContext.Categories.ToList();

      foreach (var category in categories)
      {
        category.DbContext = dbContext;
      }

      tester.SerializeAndDeserialize(categories);
    }

    [TestMethod]
    public void ISerializable()
    {
      dbContext.Configuration.ProxyCreationEnabled = false;

      var tester = new XmlDataContractSerializerTester<IEnumerable<Product>>(new NetDataContractSerializer(), true);
      var products = dbContext.Products.ToList();

      var objContext = (dbContext as IObjectContextAdapter).ObjectContext;

      foreach (var product in products)
      {
        objContext.LoadProperty(product, f => f.Order_Details);
        objContext.LoadProperty(product, f => f.Supplier);
        objContext.LoadProperty(product, f => f.Category);
      }

      tester.SerializeAndDeserialize(products);
    }


    [TestMethod]
    public void ISerializationSurrogate()
    {
      dbContext.Configuration.ProxyCreationEnabled = false;

      var selector = new SurrogateSelector();
      var streamingContext = new StreamingContext(StreamingContextStates.Persistence, null);

      selector.AddSurrogate(
        typeof(Order_Detail),
        streamingContext,
        new Order_DetailSerializationSurrogate());

      var serializer = new NetDataContractSerializer(streamingContext)
      {
        SurrogateSelector = selector
      };

      var tester = new XmlDataContractSerializerTester<IEnumerable<Order_Detail>>(serializer, true);
      var orderDetails = dbContext.Order_Details.ToList();

      var objContext = (dbContext as IObjectContextAdapter).ObjectContext;

      foreach (var orderDetail in orderDetails)
      {
        objContext.LoadProperty(orderDetail, f => f.Order);
        objContext.LoadProperty(orderDetail, f => f.Product);
      }

      tester.SerializeAndDeserialize(orderDetails);
    }

    [TestMethod]
    public void IDataContractSurrogate()
    {
      dbContext.Configuration.ProxyCreationEnabled = true;
      dbContext.Configuration.LazyLoadingEnabled = true;

      var tester = new XmlDataContractSerializerTester<IEnumerable<Order>>(
        new DataContractSerializer(typeof(IEnumerable<Order>), new DataContractSerializerSettings
          {
            DataContractSurrogate = new OrderSurrogate()
          }), true);

      var orders = dbContext.Orders.ToList();

      tester.SerializeAndDeserialize(orders);
    }
	}
}
