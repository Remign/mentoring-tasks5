using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Runtime.Serialization;

namespace Task.DB
{
  using System;
  using System.Collections.Generic;
  using System.ComponentModel.DataAnnotations;
  using System.ComponentModel.DataAnnotations.Schema;
  using System.Data.Entity.Spatial;

  [DataContract]
  public partial class Category
  {
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
    public Category()
    {
      Products = new HashSet<Product>();
    }

    [DataMember]
    public int CategoryID { get; set; }

    [Required]
    [StringLength(15)]
    [DataMember]
    public string CategoryName { get; set; }

    [Column(TypeName = "ntext")]
    [DataMember]
    public string Description { get; set; }

    [Column(TypeName = "image")]
    [DataMember]
    public byte[] Picture { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
    [DataMember]
    public virtual ICollection<Product> Products { get; set; }

    [NotMapped]
    public DbContext DbContext { get; set; }

    [OnSerializing]
    public void OnSerializing(StreamingContext context)
    {
      var objContext = DbContext as IObjectContextAdapter;
      if (objContext != null)
      {
        objContext.ObjectContext.LoadProperty(this, f => f.Products);
      }

      Console.WriteLine("OnSerializing");
    }
  }
}
